FROM ubuntu:16.04
MAINTAINER Jack Kim <jskim@querensys.com>

RUN useradd -m docker && echo "docker:docker" | chpasswd && adduser docker sudo

RUN apt-get update
RUN apt-get upgrade -y sudo
RUN apt-get install -y sudo

# environment variables
WORKDIR edgex_switcher
WORKDIR edgex_switcher/python
VOLUME /usr/bin
#ENV ROOT_DIR=edgex_switcher
#ENV APP_DIR=/edgex_switcher/python

#copy python and property files to the image
COPY * edgex_switcher/
COPY python/*.py edgex_switcher/python/

#expose core data port
EXPOSE 5000
EXPOSE 49001

# Run Regist Code and healthcheck
#RUN edgex_switcher/build_ffmpeg
RUN sh edgex_switcher/python_script.sh
#RUN python $APP_DIR/edgex_switcher_healthcheck.py

#kick off the micro service
ENTRYPOINT python edgex_switcher/python/EdgeXPost.py
ENTRYPOINT python edgex_switcher/python/edgex_switcher_healthcheck.py