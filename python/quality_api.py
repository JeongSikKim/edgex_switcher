# -*- coding: utf8 -*-

import sys
import os
from flask import Flask
from flask import request
import json
import subprocess

app = Flask(__name__)
httpHOST = '0.0.0.0'
httpPORT = 49001

# startcmd1 = 'ffmpeg -rtsp_transport tcp -i rtsp://192.168.100.41:1554/video1+audio1  -c:v h264_nvenc -c:a aac -b:a 128k -ar 44100 -f mpegts udp://localhost:50000?fifo_size=10000000'
startcmd1 = 'ffmpeg -re -stream_loop -1 -i /home/queren/work_space/1080p_conor_mcgregor.mp4 -c:v h264_nvenc -c:a aac -b:a 128k -ar 44100 -f mpegts udp://localhost:50000?fifo_size=10000000'

# startcmd2 = 'ffmpeg -rtsp_transport tcp -i rtsp://192.168.100.41:1554/video1+audio1  -r 30 -g 60 -force_key_frames "expr:gte(t,n_forced*2.0)" -b:v 200k -c:a aac -b:a 128k -ar 44100 -f mpegts udp://localhost:50000?fifo_size=10000000'
startcmd2 = 'ffmpeg -re -stream_loop -1 -i /home/queren/work_space/1080p_conor_mcgregor.mp4  -c:v h264_nvenc -r 30 -g 60 -force_key_frames "expr:gte(t,n_forced*2.0)" -b:v 200k -c:a aac -b:a 128k -ar 44100 -f mpegts udp://localhost:50000?fifo_size=10000000'

hq = {"hq": "on"}
lq = {"hq": "off"}

#@app.route("/hecas_switch", methods=['POST'])
#def HQ():
#    subprocess.call(startcmd1, shell=True)
#    return "<html><body>on</body></html>"\

#@app.route("/api/v1/devices/5bb32ce29f8fc200018bd6ae/highquality", methods=['PUT'])
@app.route("/high", methods=['PUT'])
def api_message():
    print ("input["+request.data+"]")
    msg = json.loads(request.data)
   # if request.headers['Content-Type'] == 'application/json':
    current_process = None
    if 'on' == msg['hq']:
        print("if"+request.data)
        os.system('pkill -9 -ef ffmpeg')
        if current_process is not None:
            current_process.terminate()
        current_process = subprocess.Popen(startcmd1, shell=True)
        return "JSON Message: " + json.dumps(request.json)
    elif 'off' == msg['hq']:
        print("elif"+request.data)
        os.system('pkill -9 -ef ffmpeg')
        if current_process is not None:
            current_process.terminate()
        current_process = subprocess.Popen(startcmd2, shell=True)
        return "JSON Message: " + json.dumps(request.json)
#    else:
#        os.system('pkill -9 -ef ffmpeg')
#        return "JSON Message: " + json.dumps(request.json)
    else:
        print("else"+request.data)
	return "415 Unsupported Media Type ;)"

# @app.route("/off")
# def LQ():
#     subprocess.call(startcmd2, shell=True)
#     return "<html><body>off</body></html>"
#
#
# @app.route("/stop")
# def stop():
#     os.system('pkill -9 -ef ffmpeg')
#     return "<html><body>stop</body></html>"


if __name__ == "__main__":
    # app.debug = True
    app.run(httpHOST, httpPORT)

