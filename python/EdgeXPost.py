import EdgeXRestAPI
import EdgeXDictionary

servicename = 'iec-hecas-switcher-service'
addressablename = 'iec-device-hecas-switcher-device-addressable'
deviceservicename = 'iec-device-hecas-switcher-service'
profilename = 'hecas.switcher.profile'
devicename = 'hecas-switch-device'


def initRegistrationInformations(edgex_host='http://localhost:48081/api/v1/',
                                 edgex_coredata_host='http://localhost:48080/api/v1/'):
    Service_Info_restHelper = EdgeXRestAPI.ServiceInfoRestHelper(API_HOST=edgex_host)
    Service_Value_restHelper = EdgeXRestAPI.ServiceInfoRestHelper(API_HOST=edgex_coredata_host)

    # POST Addressable
    switch_addressable = EdgeXDictionary.Addressable(name=addressablename, protocol='HTTP',
                                                     method='POST', address='129.254.171.61', port=49001,
                                                     path='/highquality',
                                                     publisher='none', user='none', password='none', topic='none')

    Service_Info_restHelper.post_addressable(switch_addressable)

    # POST Device Service
    switch_deviceservice = EdgeXDictionary.DeviceService(name=deviceservicename,
                                                         description='hecas switch device service',
                                                         labels=["hq", "value"], adminState='unlocked',
                                                         operatingState='enabled',
                                                         addressable={
                                                             "name": addressablename})

    Service_Info_restHelper.post_deviceService(switch_deviceservice)

    # POST Device Profile
    switch_deviceProfile = EdgeXDictionary.DeviceProfile(name=profilename, manufacturer='Querensys', model='switch',
                                                         description='device-switch-profile', labels=['switch'],
                                                         commands=[{"name": "HQ",
                                                                    "get": {
                                                                        "path": "/api/v1/devices/{deviceId}/highquality",
                                                                        "response": [{"code": "200",
                                                                                      "description": "Get the high quality status",
                                                                                      "expectedValues": ["hq"]
                                                                                      },
                                                                                     {"code": "503",
                                                                                      "description": "Service unavailable",
                                                                                      "expectedValues": ["cameraerror"]
                                                                                      }
                                                                                     ]
                                                                    },
                                                                    "put": {
                                                                        "path": "/api/v1/devices/{deviceId}/highquality",
                                                                        "response": [{"code": "204",
                                                                                      "description": "Set the high quality status",
                                                                                      "expectedValues": []
                                                                                      },
                                                                                     {"code": "503",
                                                                                      "description": "Service unavailable",
                                                                                      "expectedValues": ["cameraerror"]
                                                                                      }
                                                                                     ]
                                                                    }
                                                                    }])

    Service_Info_restHelper.post_deviceProfile(switch_deviceProfile)

    # POST Device Information
    switch_deviceInfo = EdgeXDictionary.DeviceInfo(name=devicename,
                                                   description='hecas switch device', adminState='UNLOCKED',
                                                   operatingState='ENABLED', addressable={'name': addressablename},
                                                   labels=['quality', 'high'], location='',
                                                   service={'name': deviceservicename},
                                                   profile={'name': profilename})
    Service_Info_restHelper.post_deviceInfo(switch_deviceInfo)

    # POST ValueDescriptor Information
    switch_ValueDescriptorInfo = EdgeXDictionary.ValueDescriptor(name='HQ',
                                                                 min='0',
                                                                 max='3000000',
                                                                 type='F',
                                                                 uomLabel='bitrate',
                                                                 defaultValue='0',
                                                                 formatting='%s',
                                                                 labels=['HighQuality'])

    Service_Value_restHelper.post_valueDescriptor(switch_ValueDescriptorInfo)


if __name__ == "__main__":
    initRegistrationInformations()
